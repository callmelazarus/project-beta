import React from 'react';

class EmployeeSalesHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales: [],
            salesPersons: [],
        }
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
    }

    async handleSalesPersonChange(event) {
        const value = event.target.selectedIndex;
        console.log(event.target.selectedIndex)
        console.log("target", event)
        const salesPersonIndividualResponse = await fetch(`http://localhost:8090/api/records/`)
        if (salesPersonIndividualResponse.ok) {
            const recordData = await salesPersonIndividualResponse.json();
            console.log("recorddata.sales", recordData.sales)
            const listofrecords = []
            for (let record of recordData.sales){
                console.log("record",record)
                console.log("salesperson",record.sales_person.id)
            if (event.target.selectedIndex == record.sales_person.id ){
                listofrecords.push(record)
            }
            }
            console.log("recordData", recordData)
            this.setState({sales: listofrecords})
        }
    }

    async componentDidMount() {
        const salesPersonResponse = await fetch("http://localhost:8090/api/sales/");
        if (salesPersonResponse.ok) {
            const salesPersonData = await salesPersonResponse.json();
            this.setState({salesPersons: salesPersonData.sales_people})
        }
    }

    render() {
        return(
            <div className="row">
                <h1>Sales Person's History</h1>
                <div className="offset-3 col-6">
                    <div className="mb-3">
                        <select onChange={this.handleSalesPersonChange} value={this.state.employee} required id="sales_person" name="sales_person" className="form-select">
                            <option value="">Choose a sales person</option>
                            {this.state.salesPersons.map(employee => {
                                return (
                                    <option key={employee.employee_number} value={employee.employee_number}>
                                        {employee.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                </div>
                
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.map((s) => {
                            return (
                                <tr key={s.id}>
                                    <td>{s.sales_person.name}</td>
                                    <td>{s.customer.name}</td>
                                    <td>{s.automobile.vin}</td>
                                    <td>{s.price}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default EmployeeSalesHistory;




