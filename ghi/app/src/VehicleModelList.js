import React from "react";
import { Link } from "react-router-dom"

class VehicleModelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicleModels: [],
    };
    //bind here
  }

  async componentDidMount() {
    const VehicleModelUrl = "http://localhost:8100/api/models/";
    const vehicleResponse = await fetch(VehicleModelUrl);
    if (vehicleResponse.ok) {
      const vehicleData = await vehicleResponse.json();
      this.setState({ vehicleModels: vehicleData.models });

      console.log("vehicle model data--", vehicleData.models);
    }
  }

  render() {
    return (
      <>
        <h1 className="mt-3">List of Vehicle Models</h1>
        <Link
          className="btn btn-primary mb-3 btn-sm"
          href="#"
          to="/inventory/createvehiclemodels"
          role="button"
        >
          Add a Vehicle model
        </Link>

        <table className="table table-bordered table-hover">
          <thead className="">
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody className="table-hover">
            {this.state.vehicleModels.map((modelinfo) => {
              return (
                <tr key={modelinfo.id}>
                  <td>{modelinfo.name}</td>
                  <td>{modelinfo.manufacturer.name}</td>
                  <td><img width="225" src={modelinfo.picture_url} /></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default VehicleModelList;
