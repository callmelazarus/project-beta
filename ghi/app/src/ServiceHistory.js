import React from "react";
import FixDateTime from "./DateTimeUpdate";

class ServiceHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
      noVin: "",
    };
    //bind functions here
    this.handleboolean = this.handleboolean.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  async handleSearch(event) {
    event.preventDefault();
    // reset the state for each subsequent search
    this.setState({ noVin: "", appointments: [] });

    const apptUrl = "http://localhost:8080/api/appointments/";
    const apptResponse = await fetch(apptUrl);
    if (apptResponse.ok) {
      const apptData = await apptResponse.json();

      // retreive the searched value
      const searchedVinInput = event.target[1].value;
      // const filteredVINlist = [];
      // for (const appt of apptData.appointments) {
      //   if (appt.vin.vin === searchedVinInput) filteredVINlist.push(appt);
      // }

      // remove apptData if VIN does not match

      const filteredVINlist = apptData.appointments.filter(
        (appt) => appt.vin.vin === searchedVinInput
      );
      console.log("filtered VINS", filteredVINlist);

      // Depending on the filtered list -> if empty, produce a message noting VIN doesnt' match
      // if list contain VIN's -> set state to show those appointments
      if (filteredVINlist.length === 0) {
        console.log("no matching VIN");
        this.setState({
          noVin:
            "Please re-enter VIN #. The VIN you entered does not match any previously serviced Vehicles",
        });
      } else {
        this.setState({ appointments: filteredVINlist });
      }
    }
  }

  // handle booleans
  handleboolean(vipstatus) {
    if (vipstatus == true) {
      return "Yes";
    } else {
      return "No";
    }
  }

  render() {
    return (
      <>
        <form onSubmit={this.handleSearch} id="search-vin">
          <div className="input-group mt-3 mb-3">
            <div className="input-group-prepend">
              <button className="btn btn-outline-primary">
                Search for a VIN #
              </button>
            </div>
            <input
              type="text"
              className="form-control"
              placeholder="Input VIN"
              aria-label=""
              aria-describedby="basic-addon1"
            />
          </div>
        </form>

        <h1 className="mt-3 mb-3">Service History for VIN</h1>
        <table className="table table-bordered table-hover">
          <thead className="">
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date and Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP Treatment</th>
              <th>Service Finished?</th>
            </tr>
          </thead>
          <tbody className="table-hover">
            {this.state.appointments.map((appt) => {
              return (
                <tr key={appt.id}>
                  <td>{appt.vin.vin}</td>
                  <td>{appt.owner_name}</td>
                  <td>
                    <FixDateTime datetime={appt.date_time} />
                  </td>
                  <td>{appt.technician.name}</td>
                  <td>{appt.reason}</td>
                  <td>{this.handleboolean(appt.vip_treatment)}</td>
                  <td>{this.handleboolean(appt.finished)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <h3>{this.state.noVin}</h3>
      </>
    );
  }
}
export default ServiceHistory;
