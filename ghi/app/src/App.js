import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./TechnicianForm";
import SalesPersonForm from "./SalesPersonForm";
import CustomerForm from "./CustomerForm";
import SaleRecordForm from "./SalesRecord";
import AppointmentForm from "./AppointmentForm";
import ListAppointments from "./AppointmentList";
import ServiceHistory from "./ServiceHistory";
import ManufList from "./ManufList";
import ServiceHomePage from "./ServiceHomePage";
import ManufacturerForm from "./ManufForm";
import VehicleModelList from "./VehicleModelList";
import VehicleModelForm from "./VehicleModelForm";
import SalesList from "./SalesList";
import SalesPersonHistory from "./SalesPersonHistory";
import AutomobileForm from "./AutomobileForm";
import AutomobileList from "./AutomobileList";
import FormComplete from "./FormCompleteModal";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/inventory">
            <Route path="listmanufacturer" element={<ManufList />} />
            <Route path="createmanufacturer" element={<ManufacturerForm />} />
            <Route path="listvehiclemodels" element={<VehicleModelList />} />
            <Route path="createvehiclemodels" element={<VehicleModelForm />} />
            <Route path="automobile/new" element={<AutomobileForm />} />
            <Route path="automobile/list" element={<AutomobileList />} />
          </Route>

          <Route path="/service">
            <Route path="" element={<ServiceHomePage />} />
            <Route path="newtechnician" element={<TechnicianForm />} />
            <Route path="newappointment" element={<AppointmentForm />} />
            <Route path="appointmentlist" element={<ListAppointments />} />
            <Route path="servicehistory" element={<ServiceHistory />} />
          </Route>

          <Route path="/tests">
            <Route path="modal" element={<FormComplete />} />
            {/* <Route path="newtechnician" element={<TechnicianForm />} /> */}
          </Route>


          <Route path="/sales">
            <Route path="" />
            <Route path="newSalesPerson" element={<SalesPersonForm />} />
            <Route path="newRecord" element={<SaleRecordForm />} />
            <Route path="list" element={<SalesList />} />
            <Route path="salespersons" element={<SalesPersonHistory />} />
            <Route path="addCustomer" element={<CustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
