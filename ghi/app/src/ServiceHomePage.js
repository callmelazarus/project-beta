import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import React, { useState } from "react";

function ServiceHomePage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Service Homepage
        </p>
        <h3>Technician Form </h3>
          <p> A page to add a new Technician </p>
        <h3>Service Appointment Form</h3>
          <p>A page to create a new Appointment </p>
        <h3>List of Appointments</h3> 
          <p>A page that shows all unfinished Service Appointments</p>
        <h3>Service History</h3>
          <p>A page where you can search for Service's past and present, based on a particular VIN</p>
      </div>
    </div>
  );
}


export default ServiceHomePage;
