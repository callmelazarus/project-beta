import React from "react";
import { Link } from "react-router-dom"

class ManufList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturers: [],
    };
    //bind here
  }

  async componentDidMount() {
    const manufUrl = "http://localhost:8100/api/manufacturers/";
    const manufResponse = await fetch(manufUrl);
    if (manufResponse.ok) {
      const manufData = await manufResponse.json();
      this.setState({ manufacturers: manufData.manufacturers });

      console.log("manuf data--", manufData.manufacturers);
    }
  }

  render() {
    return (
      <>
        <h1 className="mt-3">List of Manufacturers</h1>
        <Link
          className="btn btn-primary mb-3 btn-sm"
          href="#"
          to="/inventory/createmanufacturer"
          role="button"
        >
          Add a Manufacturer
        </Link>

        <table className="table table-bordered table-hover">
          <thead className="">
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody className="table-hover">
            {this.state.manufacturers.map((name) => {
              return (
                <tr key={name.id}>
                  <td>{name.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default ManufList;
