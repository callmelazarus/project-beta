import React from "react";

class VehicleModelForm extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      name: "",
      pictureUrl: "",
      manufacturer: "",
      manufacturers: [],
    }
    //bind here
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleChangePicture = this.handleChangePicture.bind(this)
    this.handleChangeManuf = this.handleChangeManuf.bind(this)
  }
 
  // handle state change

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handleChangePicture(event) {
    const value = event.target.value;
    this.setState({ pictureUrl: value });
  }
  handleChangeManuf(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }


    // retrieve list of manufacturers to place in dropdown

    async componentDidMount() {
      const url = "http://localhost:8100/api/manufacturers/"
      const response = await fetch(url)
  
      if (response.ok) {
        const data = await response.json()
        console.log("data about manuf", data)
        // sets state to be a list of technicians
        this.setState({manufacturers: data.manufacturers})
      }
    }

    // handle submission

    async handleSubmit(event) {
      event.preventDefault();
      const data = { ...this.state };
      data.picture_url = data.pictureUrl
      data.manufacturer_id = data.manufacturer
      delete data.manufacturers
      delete data.pictureUrl

      // console.log('data----', data)
  
      const techUrl = "http://localhost:8100/api/models/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };

      const response = await fetch(techUrl, fetchConfig);
      // console.log('response---', response)
      if (response.ok) {
        const newVehicleModel = await response.json();
        console.log("this is the new Vehicle Model", newVehicleModel);
        this.setState({
          name: "",
          picture_url: "",
          manufacturer: '',
        });
      }
    }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="p-3">Add a new Vehicle Model</h1>
            <form onSubmit={this.handleSubmit} id="create-vehiclemodel-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value = {this.state.name}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Vehicle Model Name</label>
              </div>

              <div className="form-floating mb-3">
              <input
                  onChange={this.handleChangePicture}
                  value = {this.state.pictureUrl}
                  placeholder="pictureUrl"
                  required
                  type="text"
                  name="pictureUrl"
                  id="pictureUrl"
                  className="form-control"
                />
                <label htmlFor="pictureUrl">URL of Picture of Vehicle Model</label>
              </div>

              <div className="mb-3">
                <select
                  value={this.state.manufacturer}
                  onChange={this.handleChangeManuf}
                  required
                  name="manufacturer"
                  id="manufacturer"
                  className="form-select"
                >
                  <option value="">Select the Manufacturer</option>
                  {this.state.manufacturers.map((manufacturer) => {
                    return (
                      <option
                        key={manufacturer.id} 
                        value={manufacturer.id} // using id because that is how API will receive data 
                      >
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Add a Vehicle Model</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleModelForm;
