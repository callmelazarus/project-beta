import React from 'react'

class SalesPersonForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: "",
            employeeNumber: ""
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleNumberChange = this.handleNumberChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event){
        const value = event.target.value;
        this.setState({name: value})
    }
    handleNumberChange(event){
        const value = event.target.value;
        this.setState({employeeNumber: value})
    }
    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};

        const salespersonUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                employeeNumber: "",
            };
            this.setState(cleared);
        };
    };

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="new-salesperson-form">
                            <div className="form-flaoting mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-flaoting mb-3">
                                <input onChange={this.handleNumberChange} value={this.state.employeeNumber} placeholder="Number" required type="text" name="employeeNumber" id="employeeNumber" className="form-control"/>
                                <label htmlFor="employeeNumber">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesPersonForm;
