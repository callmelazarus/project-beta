import React from "react";
import FormComplete from "./FormCompleteModal";

// function modalTest() {
//   return <FormComplete />;
// }

// create technician form
class TechnicianForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      employeeNumber: "",
      isOpen: false,
    };
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeEmployeeNum = this.handleChangeEmployeeNum.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleModalChange = this.handleModalChange.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
  }

  // handle state changes

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleChangeEmployeeNum(event) {
    const value = event.target.value;
    this.setState({ employeeNumber: value });
  }
  handleModalChange() {
    this.setState({ isOpen: true });
  }
  handleModalClose() {
    this.setState({ isOpen: false });
  }

  // handle submission

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    data.employee_number = data.employeeNumber;
    delete data.employeeNumber;
    // console.log('data----', data)

    const techUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(techUrl, fetchConfig);
    // console.log('response---', response)
    if (response.ok) {
      const newTech = await response.json();
      console.log("this is the new technician", newTech);
      this.setState({
        name: "",
        employeeNumber: "",
      });
    }
    // launch modal
    let abcde = "Technician Form complete";
    <FormComplete />;
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="p-3">Add a new Technician</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value={this.state.name}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Technician Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeEmployeeNum}
                  value={this.state.employeeNumber}
                  placeholder="employeeNumber"
                  required
                  type="text"
                  name="employeeNumber"
                  id="employeeNumber"
                  className="form-control"
                />
                <label htmlFor="employeeNumber">Employee Number</label>
              </div>
              <button className="btn btn-primary">Add Technician</button>
            </form>
            <FormComplete
              isOpen={this.state.isOpen}
              handleClose={this.handleModalClose}
              string={"Technician Form"}
            />
            <button
              onClick={this.handleModalChange}
              className="btn btn-primary"
            >
              Test Modal
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
