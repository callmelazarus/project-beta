import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-black">
      <div className="container-fluid">
        <a className="navbar-brand" href="#"></a>
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNavDarkDropdown"
          aria-controls="navbarNavDarkDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/listmanufacturer"
                  >
                    Manufacturers
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/createmanufacturer"
                  >
                    Add Manufacturer
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/listvehiclemodels"
                  >
                    Vehicle Models
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/createvehiclemodels"
                  >
                    Add Vehicle Models
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/automobile/list"
                  >
                    Automobiles
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="inventory/automobile/new"
                  >
                    Add Automobile
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink className="dropdown-item" to="sales/addCustomer">
                    Add Customer
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales/newSalesPerson">
                    Add Salesperson
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales/newRecord">
                    Create Record
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales/list">
                    List of Sales
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales/salespersons">
                    Sales Persons History
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink className="dropdown-item" to="service/newtechnician">
                    Add Technician
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="service/newappointment"
                  >
                    Add Appointment/Service
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="service/appointmentlist"
                  >
                    Show Appointments
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="service/servicehistory"
                  >
                    Service History
                  </NavLink>
                </li>
                
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
