function FixDateTime(props) {
  const { datetime } = props;

  const [rawDate, rawTime] = datetime.split("T");
  console.log(rawTime.substring(0, 5));
  let militaryTime = rawTime.substring(0, 5);

  return (
    <>
      Date: {rawDate} & Time: {militaryTime}
    </>
  );
}
export default FixDateTime;
