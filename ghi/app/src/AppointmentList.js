import React from "react";
import { NavLink, Link, Outlet } from "react-router-dom";
import FixDateTime from "./DateTimeUpdate";

class ListAppointments extends React.Component {
  constructor(props) {
    super(props);
    this.state = { appointments: [] };
    // finished:'' // should this be blank, or set to False?
    // bind functions here
    this.handleVIP = this.handleVIP.bind(this);
    this.handleClickDelete = this.handleClickDelete.bind(this);
    this.handleClickFinish = this.handleClickFinish.bind(this);
  }

  // fetch appointment information

  async componentDidMount() {
    const apptUrl = "http://localhost:8080/api/appointments/";
    const apptResponse = await fetch(apptUrl);
    if (apptResponse.ok) {
      const apptData = await apptResponse.json();
      // console.log("appointment Data, including finished appts", apptData);

      const listOfUnfinishedAppts = [];
      // remove apptData if attribute 'finished' is 'true'
      for (const appt of apptData.appointments) {
        if (appt.finished == false) {
          listOfUnfinishedAppts.push(appt);
        }
      }
      // console.log("only unfinished appointments", listOfUnfinishedAppts);
      this.setState({ appointments: listOfUnfinishedAppts });
      // console.log("state data: this.state.appointments", this.state.appointments)
    }
  }

  // handle VIP status
  // would like to return special VIP image in refactor
  handleVIP(vipstatus) {
    if (vipstatus == true) {
      return "Yes";
    } else {
      return "No";
    }
  }

  // clicking finish will engage in PUT request to change finished attribute of instance to True

  async handleClickFinish(apptId) {
    const url = `http://localhost:8080/api/appointments/${apptId}`;
    const finished = { finished: true };
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(finished),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    window.location.reload();

    if (response.ok) {
      console.log("updated Finished attribute to True");
    } else {
      console.log("Response no good");
    }
  }

  // clicking delete will engage in DELETE request of instance

  async handleClickDelete(apptId) {
    const url = `http://localhost:8080/api/appointments/${apptId}`;
    console.log("url", url);
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    window.location.reload();
  }

  render() {
    return (
      <>
        <h1 className="mt-3">List of appointments</h1>
        <Link
          className="btn btn-primary mb-3 btn-sm"
          href="#"
          to="/service/newappointment"
          role="button"
        >
          Add an Appointment
        </Link>

        <table className="table table-bordered table-hover">
          <thead className="">
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date and Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP Treatment</th>
              <th>Service Finished?</th>
              <th>Delete?</th>
            </tr>
          </thead>
          <tbody className="table-hover">
            {this.state.appointments.map((appt) => {
              return (
                <tr key={appt.id}>
                  <td>{appt.vin.vin}</td>
                  <td>{appt.owner_name}</td>
                  <td><FixDateTime datetime = {appt.date_time}/></td>
                  <td>{appt.technician.name}</td>
                  <td>{appt.reason}</td>
                  <td>{this.handleVIP(appt.vip_treatment)}</td>
                  <td>
                    <button
                      onClick={() => this.handleClickFinish(appt.id)} // use arrow fxn to pass argument to custom fxn
                      type="button"
                      className="btn btn-outline-success btn-sm"
                    >
                      Done
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => this.handleClickDelete(appt.id)} // use arrow fxn to pass argument to custom fxn
                      type="button"
                      className="btn btn-outline-danger btn-sm"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default ListAppointments;
