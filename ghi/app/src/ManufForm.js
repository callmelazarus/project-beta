import React from "react";

class ManufacturerForm extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      name: ""
    }
    //bind here
    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
 
  // handle state change

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

    // handle submission

    async handleSubmit(event) {
      event.preventDefault();
      const data = { ...this.state };

      console.log('data----', data)
  
      const techUrl = "http://localhost:8100/api/manufacturers/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };

      const response = await fetch(techUrl, fetchConfig);
      // console.log('response---', response)
      if (response.ok) {
        const newManuf = await response.json();
        console.log("this is the new Manufacturer", newManuf);
        this.setState({
          name: "",
        });
      }
    }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="p-3">Add a new Manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value = {this.state.name}
                  placeholder="name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Add a Manufacturer</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ManufacturerForm;
