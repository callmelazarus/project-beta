import React from "react";
// import { useNavigate } from "react-router-dom";

// // Wrap and export
// export default function(props) {
//   const navigation = useNavigation();

//   return <AppointmentForm {...props} navigation={navigation} />;
// }

// create Appointment form
class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ownerName: "",
      dateTime: "",
      reason: "",
      technicians: [],
      vin: "",
    };
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDateTime = this.handleChangeDateTime.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // handle state changes

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ ownerName: value });
  }
  handleChangeDateTime(event) {
    const value = event.target.value;
    this.setState({ dateTime: value });
  }
  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }
  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }
  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  // handle submission
  // convert the properties from camelCase to snake_case

  // need to convert date time (react) to dateTime (django)
  // react format: "2022-10-10T10:10"
  // django format: "2022-10-10 T 01:08:27+00:00"

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    data.owner_name = data.ownerName;
    data.date_time = data.dateTime;
    data.date_time = data.date_time + ":00+00:00"; // convert date_time to fit django
    delete data.ownerName;
    delete data.dateTime;
    delete data.technicians;
    // const { navigation } = this.props;

    const apptUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(apptUrl, fetchConfig);

    if (response.ok) {
      const newAppt = await response.json();
      console.log("this is the new Appointment", newAppt);
      this.setState({
        ownerName: "",
        dateTime: "",
        reason: "",
        technician: "",
        vin: "",
      });
      // navigation("/service/appointmentlist");
    } else {
      console.log("Post request for new appointment no good");
    }
  }

  // retrieve list of technicians to place in dropdown

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      // console.log("data about techs", data)
      // sets state to be a list of technicians
      this.setState({ technicians: data.technicians });
    }
  }

  // render page

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="p-3">Create an Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value={this.state.ownerName}
                  placeholder="ownerName"
                  required
                  type="text"
                  name="ownerName"
                  id="ownerName"
                  className="form-control"
                />
                <label htmlFor="ownerName">Owner Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeDateTime}
                  value={this.state.dateTime}
                  placeholder="dateTime"
                  required
                  type="datetime-local"
                  name="dateTime"
                  id="dateTime"
                  className="form-control"
                />
                <label htmlFor="dateTime">Date and Time of Appointment</label>
              </div>

              <div className="mb-3">
                <label htmlFor="reason">Reason for Appointment</label>
                <textarea
                  onChange={this.handleChangeReason}
                  value={this.state.reason}
                  className="form-control"
                  id="reason"
                  required
                  name="reason"
                  rows="3"
                ></textarea>
              </div>

              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeVin}
                  value={this.state.vin}
                  placeholder="vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">VIN Number</label>
              </div>

              <div className="mb-3">
                <select
                  value={this.state.technician}
                  onChange={this.handleChangeTechnician}
                  required
                  name="technician"
                  id="technician"
                  className="form-select"
                >
                  <option value="">Choose a Technician for the job</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create Appointment</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
