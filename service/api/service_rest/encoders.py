
# inherit from ModelEncoder, located in common/json
from common.json import ModelEncoder

from .models import Technician, Appointment, AutomobileVO

# encodes model info into JSON string


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "inventory"
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number"
    ]

# need to refine this
class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "owner_name",
        "date_time",
        "reason",
        "technician",
        "vin",
        "vip_treatment",
        "finished",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "vin": AutomobileVOEncoder()
    }




