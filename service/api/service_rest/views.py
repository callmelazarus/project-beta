from tkinter import EW
from xml.etree.ElementInclude import FatalIncludeError
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Appointment, AutomobileVO, Technician

from .encoders import AppointmentEncoder, TechnicianEncoder, AutomobileVOEncoder

from django.forms.models import model_to_dict


# get list of AutomobileVO's
@require_http_methods(["GET"])
def api_automobilevo(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVOs": cars},
            encoder=AutomobileVOEncoder,
        )


# Get a list of technicians, and POST technician.

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else: # POST technician
        try:
            content = json.loads(request.body) ## decodes JSON into python dictionary
            technician = Technician.objects.create(**content) # django takes dictionary, and converts into kwargs
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


# API for appointments
# Get list of appointments
# will need to Create appointment, with a number of details


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET": 
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else: # POST a detail for an appointment
        try:
            content = json.loads(request.body)
            print('content----- before VIP treatment block', content)

            # get technician information
            try:
                technician = Technician.objects.get(id=content['technician'])
                content['technician'] = technician
                # print('content at tech', content) # this works
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "invalid technician id"},
                    status=400,
                )

            # check to see if VIN is part of inventory
            # if it already exists -> assign that oldcar object into the content's 'vin' attribute
            # if it is new -> create a newcar object, and assign it to content's 'vin' attribute
            # I am using another encoder with Appointments Encoder, to encode the AutomobileVO

            # edge case : 2nd servicing, of a car that is not VIP, but exists already in AutoVO **covered and tested**

            try: # car that exists in inventory
                oldcar = AutomobileVO.objects.get(vin=content['vin'])
                automobile_dict = model_to_dict(oldcar) # converts oldcar model to dictionary
                in_inventory = automobile_dict['inventory'] # extract if inventory is T/F
                if in_inventory == True: # owner purchased from dealer
                    content['vip_treatment'] = True
                    content['vin'] = oldcar
                else: # owner did not purchase from dealer
                    content['vip_treatment'] = False
                    content['vin'] = oldcar
                print('content - true', content)

            except AutomobileVO.DoesNotExist: # if this is a new car
                print('her')
                content['vip_treatment'] = False
                newcar = AutomobileVO.objects.create(
                    vin = content['vin'],
                    inventory = False
                )
                content['vin'] = newcar
                print('content - false', content, 'newCar created:', newcar)
            content['finished'] = False

            print('content----- after VIP treatement block', content)

            # this next line creates and appt based on the request.body
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe = False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response



# update appointment (provide flexibility for total updates, or just finihsed (easier))
# delete/cancel an appointment 

@require_http_methods(["PUT", "DELETE"])
def api_appointment(request, pk):
    if request.method == "PUT":
        try:
            content = json.loads(request.body) 
            print('put request content-----', content)
            appointment = Appointment.objects.get(id=pk)
            print('appt', appointment)

            # limits property updates to 'finished' attribute
            properties = 'finished'
            
            if properties in content:
                # setattr(object , which attribute to be assigned, value of the adjusted attribute)
                setattr(appointment, properties, content[properties])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe = False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "appt does not exist"})
            response.status_code = 404
            return response 
    else: # DELETE
        try: 
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse({"message": "Appointment deleted"})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appt does not exist"})
