from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
  vin = models.CharField(max_length=17, unique=True)
  inventory = models.BooleanField()

  def get_api_url(self):
      return reverse("api_automobilevo", kwargs={"pk": self.id})

# setup urls.py first, and views.py to define first parameter
# need to refine path
  # def get_api_url(self):
  #     return reverse("api_automobile", kwargs={"pk": self.id})

class Technician(models.Model):
  name = models.CharField(max_length=100)
  employee_number = models.IntegerField(unique=True)

# setup urls.py first, and views.py to define first parameter
# need to refine 
  def get_api_url(self):
      return reverse("api_technicians", kwargs={"pk": self.id})

# the name of the person to whom the vehicle belongs, 
# the date and time of the appointment, 
# and a reason for the service appointment
# the assigned technician, 
# VIN of the vehicle, 

class Appointment(models.Model): 
  owner_name = models.CharField(max_length=100)
  date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
  # date = models.DateField(auto_now=False, auto_now_add=False)
  # time = models.TimeField(auto_now=False, auto_now_add=False)
  reason = models.TextField()
  technician = models.ForeignKey(Technician, related_name='appointment', on_delete=models.PROTECT) 
  vin = models.ForeignKey(AutomobileVO, related_name='appointment', max_length=17, on_delete=models.PROTECT)
  vip_treatment = models.BooleanField(default=False, blank=True)
  finished = models.BooleanField(default=False, blank=True)

# setup urls.py first, and views.py to define first parameter
  def get_api_url(self):
      return reverse("api_appointments", kwargs={"pk": self.id})
