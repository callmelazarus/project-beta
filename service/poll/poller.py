import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something
# from service.api.service_rest.models import AutomobileVO # this strangely doesn't work..
from service_rest.models import AutomobileVO

# Service microservice polls Inventory microservice for automobile data
def get_automobile():
    automobile = "http://inventory-api:8000/api/automobiles/"
    response = requests.get(automobile)
    content = json.loads(response.content)

    # loop thru various autos
    # need to create a AutomobileVO (update or create)
    for cars in content['autos']:
        object_info = AutomobileVO.objects.update_or_create(
            vin = cars['vin'],
            inventory = True
        )
        # print(object_info) # tuble (object, and T/F if created)

def poll():
    while True:
        try:
            print('Service poller polling for automobile data')
            get_automobile()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
