![carcar Logo](./images/logo.jpg)

---

### Purpose

A webpage that provides a platform for car dealerships to manage their Business. This webapp can be seen as three different compartments, each corresponding to their respective business counterparts. Vehicle Inventory, Sales, and Service.

With this webapp, dealerships can manage their vehicle inventory, salespeople and technicians, customers, sales records, and service appointments.

Tech used: Front end: React, Bootstrap. Back end: Django, postgres database.

### Installation and getting started

1. git clone project from GitLab
2. Turn on Docker Desktop
3. Change directories into root directory of project "Project-Beta"
4. In terminal, while in this root directory. Build your Docker Container by typing:
   ```
   docker compose build
   ```
5. Run your container with this command. After your image is built on your computer, you can use this 'compose up' command to spin up your containers to access the site.
      ```
   docker compose up
   ```
6. In browser, type in: 'http://localhost:3000/' to enter into mainpage of CarCar site
7. Start populating the inventory with Manufacturers, Models, and Autombiles
   1. Using the Nav bar at the top of the screen, start by adding a Manufacturer
   2. Then add a model of car for that manufacturer. You can see your added Model in 'Vehicle Models' link.
   3. Then add an Automobile for that model. You can see your added model in the 'Vehicle Models' link.
      1. https://randomvin.com/ <- site to generate some random VIN #'s
      2. Keep at least one VIN # for reference to be used in other parts of website
   4. Repeat these three steps a few times to populate the database with car information.
8. If you would like to access the django admin page, you can do so after creating a superuser within each container.
   1. To create a superuser, open your docker deckstop, and make sure you have your containers running. Select the Terminal button see image below). This will pull up the terminal within that container.
      ![Terminal button on docker desktop](./images/terminal-button.jpg)
   2. Type `python manage.py createsuperuser` -> and fill in the superuser information.
   3. If you do that for the Inventory-api, service-api, and sales-api -> you can now access the admin page and interface with the database.
      - You can enter the following URL's to access the respective admin pages
        Inventory: http://localhost:8100/admin/
        Service: http://localhost:8080/admin/
        Sales: http://localhost:8090/admin/

**Follow these steps to understand the functionality of Services**
Using the NavBar -> click the 'Service' dropdown to see various pages.

1. Add at least (2) Technicians
2. Add at least (4) Appointments. Create (1) with a VIN # that matches the VIN from the Inventory, and another unique VIN #. Create (2) appointments where the VIN numbers are the same. You will see the purpose for this later on in Service History. Create (1) appointment with the 'Owner Name' set to 'test'.
3. Go to the 'Show Appointments' page to see your added appointments.
4. You should notice that the VIP treatment should note 'YES' for the VIN # that you used from the Inventory Page
5. Select 'Done' For 'Service Finished?' for (1) of the (2) appointments that have matching VIN #'s. The page should refresh, and that appointment should disappear.
6. Click Delete on the appointment with 'Owner Name', 'test'. The page should refresh, and that appointment should disappear.
7. Go to the 'Service History tab' which will show you the appointments/services for a given VIN #.
8. Enter the Vin # you copied the Inventory, into the search bar and hit enter.
   1. You should see the appointment associated with that VIN, and the VIP treatment noted as 'yes'.
9. Enter the VIN # that you made (2) appointments for. You will see both appointments in the dropdown table.
10. Enter your name in the VIN Searchbar, and see what happens :)

---

### Team:

- Person 1 - Michael - Auto Sales
- Person 2 - Jay - Automobile Service

---

## Design

Diagram of application:
![carcar Diagram](./images/carcardiagram.png)

## _For design insight for each microservice, see sections below:_

## Inventory Microservice

**Manufacturers**

- create JSX files files to display list of manufactuers, as well as another page to show the form to input a new manufactuer.

**VehicleModel**

- create JSX files files to display list of Vehicle Model, as well as another page to show the form to input a new manufactuer. Dropdown list created to select specific manufactuer

**Automobile**

- create JSX files files to display list of Vehicle Model, as well as another page to show the form to input a new manufactuer. Dropdown list created to select specific model.

**NavBar**

- Together, we decided on a particular look for the NavBar, and used a base template from Bootstrap to create dropdown links for the Inventory, Sales, and Service

---

## Service microservice

### **Back end: Django**

**MODELS:**

- Setup the models for the Technician, Appointment, and AutomobileVO
- Added application to settings.py (service/api/service_project/settings.py)

_AutomobileVO: attributes -> VIN, and Inventory_

- Poller.py polls the Inventory microservice for any new Automobiles, and creates and instance of the corresponding Automobile as a new AutomobileVO. This AutomobileVO instance is an object that can now be used within the Service Microservice
- Inventory attribute is a T/F boolean, which demarks if this AutomobileVO was retrieved from the inventory (mark True), or if this was a new AutomobileVO created within the Service Microservice

_Technician: attributes -> name and employee number_

_Appointment: attributes -> Car owner name, date/time of appointment, reason for appointment, technician, VIN #, VIP treatment, Finished_

- Technicians has a one:many relationship with appointments. One technician can be assigned to many appointments.
- VIN also has a one:many relationships with appointments. One car (VIN) can have many services throughout its lifetime
- VIP_treatment and Finished are BooleanField (T/F)
- Finished attribute is used in the Appointment List page -> to be able to note if an appointment has been complete

**VIEWS: API functions in views.py**

- setup encoders.py as a seperate file, which will be imported in views.py
  - encoders will encode our queryset objects into JSON strings
- api_automobilevo
  - Gets a list of the AutomobileVO instances
- api_technicians
  - Gets a list of technicians
  - Creates a technician
- api_appointments
  - Gets a list of appointments
  - Creates an appointment
    - searches for a technician based on the form input, and assigns that to the appointment
    - Searches AutomobileVO to see if this already exists in the inventory, and if the inventory attribute is set to True (which would occur if the instance was received via the poller, meaning it was purchased from dealership).
      - if VIN exists in inventory, it gets that specific car, and assigns that to the 'vin' attribute of the appointment, and sets the 'vip_treatment' attribute to True (because car was purchased by this dealership)
      - if car doesn't exist in inventory -> create an AutomobileVO instance, and set the 'inventory' attribute to 'False'
      - edge case is covered, if car is on it's 2nd or more servicing, and was NOT purchased from dealership. That car will still not receive VIP status.
- api_appointment
  - Updates a specific appointment
    - Allow for the appointment to be updated as 'finished'
    - no other attributes are updatable
  - Deletes a specific Appointment

**URLS: setup endpoints via urls.py**
Follows RESTful styling

- Project URL's setup to set the path for api's using "api/", including the URLS located in the service application
  - service_project/urls -> include 'service_rest.urls'
- setup service_rest.urls
  - setup API endpoints for technicians, appointments and automobileVO

**POLLER: setup poller.py (service/poll) in order to poll inventory producer to retreive VIN information**

- poll Inventory microservice
- location_url = "http://inventory-api:8000/api/automobiles/"
- if Automobile is found in Inventory microservice, update_or_create an AutomobileVO instance in the service microservice. For this new AutomobileVO instance, assign the 'vin' from the Automobile microservice, and assign the value 'True' to the inventory attribute, noting that this instance is something that was purchased from the dealership.

### Front end: React
- Updated CORS (installed apps, and middleware)

**Technician Form**

- Class component.
- This is a form to create new technician

**Service Appointment**

- Class Component.
- This is a form to create new appointment
- Drop down used to select a technician that is already in database.
- Needed to adjust date/time before posting to API, as the backend retreives Date/Time in a different fashion than this form generates.

- **List of Appointments**

- class component
- Renders a table of unfinished appointments
- using componentDidMount to fetch appointment data
- Created handleVIP method to convert Booleans to displayable String on page
- Delete Button
  - connected with Delete Method in Views.py.
- Finish button (challenging) - allows you to switch the service appointment to 'Finished'
  - Connected with PUT method n Views.py, the PUT method is restricted to update only the 'finished' attribute.
  - Clicking the 'Done' button will send a PUT request to change 'finished' status to True
  - When the appointment is set to Finished = True -> that appointment will no longer show up on list
    - Within componentDidMount -> I filtered the appointment data to discard 'finished' appointments, so that when I map thru the appointments in the render/return, I am only showing the unfinished appointements.
- Added button to Add an appointment
  - import { Link } from "react-router-dom"
  - inlieu of anchor tag, used Link Tag, and 'to' attribute, which are specific to JSX
- STRETCH GOAL: add button to link VIN numbers to service history page
- STRETCH GOAL: Dropdown button for VIN,that gives you info about VIN

**Service History: Shows you Appointments based on VIN #**

- Class component.
- Add search per VIN at top of page for user to input a VIN #
- attributes shown: VIN, Customer Name, Date and Time, Technician, Reason, VIP treatment, Service Finished?
- setup onSubmit attribute on form, to fetch all appointment data
  - Extract search input using: event.target[1].value
  - Fetch all appointments, and filter based on VIN# input in search bar
    - If filtered list was empty, assigned state to 'noVin' to output message that VIN doesnt exist
    - If filtered list was not empty, update appointments state to contain the filtered VIN list.
    - render/return the table, very similarly to how List of Appointments are setup
- Everytime a search was input, at the top of the handleSearch, I've reset the state of noVin and appointements. This refreshing of the state was necessary for subsequent searches

- STRETCH GOAL: have additional dropdown button to search based on VIN numbers
- https://getbootstrap.com/docs/4.0/components/input-group/#buttons-with-dropdowns

* Added DateTimeUpdate.js to create a function used by both the Appointment List and Service History, to show make the date, time more readable
  STRETCH GOAL: change military time to 12 hour time

### misc notes

1. started by creating branches
2. Setup Insomia to interact with Inventory API
3. Setup models.py (need to make migrations)

---

## Sales microservice

## Getting Started

1. Fork and clone project repo
2. Run commands to start and run our docker containers
3. Visit CarCar site and review code to find area to start working on project


## Creating Sales microservice

1. Add a sales person
You need to create a form that allows a person to enter the name and employee number for a sales person.
2. Add a potential customer
You need to create a form that allows a person to enter the name, address, and phone number for a potential customer. 
3. Create a sale record
You need to create a form that associates an automobile that came from inventory and has not yet been sold, a sales person, and a customer with a price to record the sale of an automobile.
4. List all sales
You need to show a page that lists all sales showing the sales person's name and employee number, the purchaser's name, the automobile VIN, and the price of the sale.
5. List a sales person's sales history
You need to show a list of the sales for a sales person's employee number.


## Create Models

Sales Person Model:
1. Name
2. Employee
   
AutomobileVO:
1. Color
2. Year
3. Vin

Customer:
1. name
2. address
3. phone number

Sales Record:
1. sales person (foreign key)
2. automobile (foreign key)
3. customer (foreign key)
4. price


## AutomobileVO
The AutomobileVO model is how the sales microservices communicates with the Automobile model in
the Inventory microservice's model. This is required to obtain the information needed for both the 
sales and service microservices. We will mainly be working with the VIN numbers of the Automobiles.



## Create Views and Urls backend

1. Created create_customer, create_sales_person, 
list_sales_api,view_customer, show_sale_api, show_sales_person views for backend
2. Assign url paths to all functions



## Front End

Add Sales Person: 
  Create a form in which a sales person can create a unique indentifier on the website.

Add Customer:
  Allows the user to enter a customer which also have their own unique indentifiers.

Create a Sale Record:
  This is a form that allows the user to enter a given sales person, customer, autmobile,
  and sales price to create a record holding that information.

List of Sale Person's Records:
  This is creating a list of sales based on that specific sales person's history.

List all sales:
  This is just a list recording all sales made on the site including, sale person,
  customer, employee number, automobile VIN, and price of sale that was made.

# Images for site reference:
[Image Storage](./images)

