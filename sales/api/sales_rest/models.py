from django.db import models
from django.core.validators import RegexValidator

class AutomobileVO(models.Model):
    color = models.CharField(max_length=20)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employeeNumber = models.IntegerField()


class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    phone_number = models.BigIntegerField()


class SalesRecord(models.Model):
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="records",
        on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="records",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name="records",
        on_delete=models.PROTECT
    )
    price = models.IntegerField()
