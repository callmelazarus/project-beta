from enum import auto
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import AutomobileVO, SalesPerson, SalesRecord, Customer
from django.http import JsonResponse
from .encoders import CustomerEncoder, SalesPersonEncoder, SalesRecordEncoder, AutomobileVOEncoder
from django.forms.models import model_to_dict









@require_http_methods(["GET", "POST"])
def create_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message" : "Creating customer failed"},
                status = 400,
            )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def view_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: 
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = ['name', 'address', 'phone_number']
            for prop in props:
                if prop in content:
                    setattr(customer, prop, customer[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )



@require_http_methods(["GET", "POST"])
def create_sales_person(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message" : "Creating Sales Person Failed"},
                status = 400,
            )
            return response



@require_http_methods(["GET", "POST"])
def list_sales_api(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        print("hello")
        content = json.loads(request.body)
        print("content--------------", content)
        try:
            vin = content['vin']
            print('vin', vin)
            del content['vin']
            automobile = AutomobileVO.objects.get(vin=vin)
            print("-----",automobile)
            content["automobile"] = automobile
            
            employee_ID = content["employee_id"]
            del content['employee_id']

            sales_person = SalesPerson.objects.get(id=employee_ID)
            content["sales_person"] = sales_person
            print(sales_person)
        
            id = content["customer_id"]
            del content['customer_id']
            customer = Customer.objects.get(id=id)
            content["customer"] = customer
            print("CONTENT-------",content)

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,

            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Record creation failed"},
                status=400,
            )


@require_http_methods(["GET"])
def show_sale_api(request, pk):
    if request.method == "GET":
        try:
            sale = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            sale = SalesRecord.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            sale = SalesRecord.objects.get(id=pk)

            props = ["sales_person", "automobile", "customer", "price"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )




@require_http_methods(["DELETE", "GET", "PUT"])
def show_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: 
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)

            props = ['name', 'employee_ID']
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, sales_person[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
