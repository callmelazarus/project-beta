from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "vin"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'address',
        'phone_number',
        'id'
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'name',
        'employeeNumber',
        "id"
    ]
   

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'automobile',
        'sales_person',
        'customer',
        'price',
        'id'
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder()
    }

