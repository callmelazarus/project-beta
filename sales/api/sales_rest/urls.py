from django.urls import path
from .views import create_customer, create_sales_person, list_sales_api,view_customer, show_sale_api, show_sales_person





urlpatterns = [
    path("customers/", create_customer, name="create_customer"),
    path("customers/<int:pk>", view_customer, name="view_customer"),
    path("sales/", create_sales_person, name="create_sales_person"),
    path("sales/person/<int:pk>", show_sales_person, name="create_sales_person"),
    path("records/", list_sales_api, name="list_sales_api"),
    path("records/<int:pk>", show_sale_api, name="list_sales_api")



]
